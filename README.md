Alpha Network


For Running client code


Enter into Alpha_client_code directory

1-npm install

2-npm start


For Running Server Code

create virtual environment using 

1-sudo apt-get install python3-pip

2-sudo pip3 install virtualenv

3-virtualenv venv

4-source venv/bin/activate

Put the Alpha_server_code folder inside (venv) folder  --**Note make sure virtual env is activated**--

Remove existing migratins folder inside Alpha_server_code directory

Install dependencies using


1-pip3 install -r requirements.txt


initiate database

1-python3 manage.py db init

create migrations (creates a migrations folder in project dir)

2-python3 manage.py db migrate

to recreate db

3-python3 manage.py create_db

to create table admin

4-python3 manage.py create_admin

to create table data

5-python3 manage.py create_data

to run server 

6-python3 manage.py runserver


**NOTES**

to flush contents of database when changes made in server side

1.python3 manage.py flush

2.re-run commands from **3**






