import React from 'react';
import Handsontable from 'handsontable';
import {  Label, Menu, Table } from 'semantic-ui-react'
import axios from 'axios';
import moment from 'moment'
import {Modal,Button} from 'react-bootstrap';
import { BaseUrl } from '../constants/BaseUrl'

const ReactDataGrid = require('react-data-grid');
const headerRow = [
    '',
    'Variable',
    'Lower Bound',
    'Upper Bound',
]

const renderBodyRow = ({ id, variable, lowerbound, upperbound }, i) => ({
    key: id || `row-${i}`,
    cells: [
        id || '',

        variable || 'No name specified',
        lowerbound
            ? { key: 'lowerbound', icon: 'attention', content: lowerbound }
            : 'Unknown',
        upperbound
            ? { key: 'upperbound', icon: 'attention', content: upperbound, warning: false }
            : 'None',
    ],
})

export class PanelBlock extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            allData: '',
            myValue: '',
            rowIndex: '',
            columnIndex: '',
            userData: '',
            deletemodalIsOpen: false,
            alphaGettingOneData: [],
            alphaGettingTwoData: [],
        },
            this.getAlphaFirstData = this.getAlphaFirstData.bind(this);
        this.getAlphaSecondData = this.getAlphaSecondData.bind(this);
        this.handleChange = this.handleChange.bind(this);
        this.enterValue = this.enterValue.bind(this);
        this.submitValue = this.submitValue.bind(this);
        this.getInitialState=this.getInitialState.bind(this);
        this.open=this.open.bind(this);
        this.close=this.close.bind(this);
    }

    componentWillMount() {
        this.getAlphaFirstData();
        this.getAlphaSecondData();
    }

    componentDidMount() {
        var values = [
            { variable: "Revenue Growth", lower_bound: '', upper_bound: '' },
            { variable: "Gross Margin", lower_bound: '', upper_bound: '' },
            { variable: "Operating Margin", lower_bound: '', upper_bound: '' },
        ];
        var container = document.getElementById('example1');
        var hot = new Handsontable(container, {
            data: values,
            rowHeaders: true,
            colHeaders: [['Variable'], ['Lower Bound'], ['Upper Bound']],
            afterSelection: function (r, c) {
                localStorage.setItem("r", r);
                localStorage.setItem("c", c);
            }
        });
        this.setState({ myValue: hot });
    }


    handleChange(e) {
        this.setState({ userData: e.target.value });
    }

    getAlphaFirstData() {

        axios({
            method: 'get',
            url: BaseUrl.RestUrl + '/alphaData'

        }).then(response => {
            if (response) {
                var data = [];
                for (var i = 0; i < response.data.length; i++) {
                    if (moment(response.data[i].date).format('YYYY-MM-DD') == '2017-07-17') {
                        data.push({
                            id: response.data[i].id,
                            variable: response.data[i].variable,
                            lowerbound: response.data[i].lowerbound,
                            upperbound: response.data[i].upperbound,
                        })
                    }
                }
            }
            this.setState({
                alphaGettingOneData: data
            })
        });
    }

    getAlphaSecondData() {

        axios({
            method: 'get',
            url: BaseUrl.RestUrl + '/alphaData'

        }).then(response => {
            if (response) {
                var data = [];
                for (var i = 0; i < response.data.length; i++) {
                    if (moment(response.data[i].date).format('YYYY-MM-DD') == '2017-03-09') {
                        data.push({
                            id: response.data[i].id,
                            variable: response.data[i].variable,
                            lowerbound: response.data[i].lowerbound,
                            upperbound: response.data[i].upperbound,
                        })
                    }
                }
            }
            this.setState({
                alphaGettingTwoData: data
            })
        });
    }

    enterValue() {
        var row = parseInt(localStorage.getItem('r'));
        var column = parseInt(localStorage.getItem('c'));
        var data = this.state.userData;
        this.state.myValue.setDataAtCell(row, column, data);
        localStorage.setItem('r', '');
        localStorage.setItem('c', '');
    }

    submitValue() {
        var allData = this.state.myValue.getSourceData();

        for (var i = 0; i < allData.length; i++) {

            var oppVal = (Math.random()) + i;
            const formData = new FormData();
            formData.append('lowerbound', allData[i].lower_bound);
            formData.append('upperbound', allData[i].upper_bound);
            formData.append('date', "2017-07-17");
            formData.append('company', "Andy Oberset");
            formData.append('expert', oppVal);
            formData.append('variable', allData[i].variable);

            axios({
                method: 'post',
                url: BaseUrl.RestUrl + '/alphaData',
                data: formData

            }).then(response => {
                console.log("response>>>>>>" + JSON.stringify(response));
                if (response.data) {
                    this.setState({ showModal: false });
                    this.getAlphaFirstData();

                }
            });
        }
    }


    getInitialState() {
        return { showModal: false };
      }
    
      close() {
        this.setState({ showModal: false });
      }
    
      open() {
        this.setState({ showModal: true });
      }

    render() {
        return (

            <div className="container" style={{ marginTop: "-20px" }} >
                <div className="row">
                    <div className="col-md-4" style={{ backgroundColor: '#efefff' }}>
                        <br></br>
                        <b style={{ color: '#000000' }}>Andy Oberset</b>
                        <br></br> <br></br>
                        <b style={{ color: '#000000' }}> Your Current Estimate for 21 Nov 2017 </b>
                        <br></br> <br></br>
                        <div id="example1" >
                        </div>
                        <br></br>
                        <b style={{ color: '#000000' }}> Estimate from 17 Jul 2017</b>
                        <Table
                            celled
                            headerRow={headerRow}
                            renderBodyRow={renderBodyRow}
                            tableData={this.state.alphaGettingOneData}
                            style={{ backgroundColor: "white" }}
                        />
                        <br></br>

                        <b style={{ color: '#000000' }}> Estimate from 09 Mar 2017</b>

                        <Table
                            celled
                            headerRow={headerRow}
                            renderBodyRow={renderBodyRow}
                            tableData={this.state.alphaGettingTwoData}
                            style={{ backgroundColor: "white" }}
                        />
                        <br></br> <br></br> <br></br> <br></br><br></br>
                    </div>
                    <div className="col-md-8" style={{ backgroundColor: '#e4e2df' }} >
                        <br></br>
                        <b style={{ color: '#000000' }}>Input</b>
                        <br></br> <br></br>
                        <p style={{ color: '#000000' }}>Please click on the cell you would like to change in the current Estimates Table</p>
                        <br></br> <br></br>
                        <div className="col-xs-3">
                            <input type="text" onChange={this.handleChange} style={{ height: "41px" }} />
                        </div>
                        <br></br> <br></br> <br></br> <br></br>
                        <div className="col-xs-3">
                            <button className="btn btn-primary" onClick={this.enterValue} style={{ width: '216px', backgroundColor: 'rgb(61, 159, 239)', color: 'white' }} >Enter EstimatE</button>
                            <button className="btn btn-primary" onClick={this.open} style={{ width: '216px', backgroundColor: 'rgb(61, 159, 239)', color: 'white' }} ><i className="fa fa-paper-plane-o" ></i>Submit a Value</button>
                        </div>
                        <Modal show={this.state.showModal} onHide={this.close}>
                        <Modal.Header style={{backgroundColor:"rgb(61, 159, 239)"}} closeButton>
                          <Modal.Title style={{color:"white", paddingTop:"9px"}} > Sure, You Want to Submit?</Modal.Title>
                        </Modal.Header>
                      
                        <Modal.Footer>
                        <Button  style={{backgroundColor:"#e6e1e0"}}  onClick={this.close}>Close</Button>
                          <Button style={{backgroundColor:"#bce0bb"}} onClick={this.submitValue}>Submit</Button>
                        </Modal.Footer>
                      </Modal>
                       
                        <br></br>  <br></br>  <br></br>  <br></br>  <br></br>  <br></br>  <br></br>  <br></br>  <br></br>  <br></br>  <br></br>  <br></br>  <br></br>  <br></br>  <br></br>  <br></br>  <br></br>  <br></br>  <br></br>  <br></br>  <br></br>  <br></br>
                    </div>
                    
                </div>
            </div>
        );
    }
}