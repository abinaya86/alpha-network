import React from 'react';
import { connect } from 'react-redux';
import '../../asset/css/plugins.min.css';
import '../../asset/css/style.themed.css';
import '../../asset/css/preload.css';
import '../../asset/css/style.light-blue-500.min.css';
import '../../asset/css/ng2-select.css';
import { Navigation } from '../navigation';
import { PanelBlock } from '../panelblock';

class Home extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
        }
    }

    componentWillMount() {
    }

    render() {
        return (
            <div>
                <Navigation />
                <PanelBlock />
            </div>
        );
    }
}

function mapStateToProps(state, props) {
    return {

    };
}

function mapDispatchToProps(dispatch) {
    return {

    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Home);