import React from 'react'
import ReactDOM from 'react-dom'
import { Provider } from 'react-redux'
import { Router, Route, browserHistory } from 'react-router'
import Home from './components/home/home'
import configureStore from './store/configure-store';
const store = configureStore();

ReactDOM.render(
  <Provider store={store}>
    <div>
      <Router history={browserHistory}>
        <Route path='/' component={Home} />
      </Router>
    </div>
  </Provider>,
  document.getElementById('root')
)
